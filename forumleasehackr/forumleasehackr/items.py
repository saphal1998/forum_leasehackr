# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field


class ForumleasehackrItem(scrapy.Item):
    username = Field()
    url = Field()
    joinedAt = Field()
    lastSeenAt = Field()
    invitedBy = Field()
    status = Field()
    profileViewCount = Field()
    lastPostedAt = Field()
    timeRead = Field()
    trustLevel = Field()
    badgeCount = Field()


class BadgeInfo(scrapy.Item):
    url = Field()
    id = Field()
    listable = Field()
    manuallyGrantable = Field()
    multipleGrant = Field()
    name = Field()
    grantCount = Field()
    description = Field()
    badgeGroupingId = Field()
    badgeTypeId = Field()


class BadgeDetails(scrapy.Item):
    allowTitle = Field()
    badgeGroupingId = Field()
    badgeTypeId = Field()
    description = Field()
    enabled = Field()
    grantCount = Field()
    id = Field()
    listable = Field()
    longDescription = Field()
    manuallyGrantable = Field()
    multipleGrant = Field()
    name = Field()
