# -*- coding: utf-8 -*-
import scrapy
import pandas as pd
import json
from forumleasehackr.items import BadgeInfo

usernames = pd.read_csv('/Users/saphalpatro/Desktop/leasehackr/MembersData_processed.csv', sep=',', encoding='utf-8')

usernames = usernames["Username"]
# print(usernames)

# Run "scrapy crawl badgesData -t csv -o badgesData.csv --loglevel=INFO" from terminal after reaching spider folder to save files


class BadgesdataSpider(scrapy.Spider):
    name = 'badgesData'
    allowed_domains = ['forum.leasehackr.com']
    start_urls = ['https://forum.leasehackr.com/']

    def start_requests(self):
        for user in usernames:
            userBadges = self.start_urls[0] + '/user-badges/' + user.lower() + '.json'
            yield scrapy.Request(userBadges, callback=self.parse_userBadges)

    def parse_userBadges(self, response):
        self.logger.info('Recieved badge response from {}'.format(response.url))
        jsonresponse = json.loads(response.body)
        for badge in range(len(jsonresponse["badges"])):
            item = BadgeInfo()
            item["id"] = jsonresponse["badges"][badge]["id"]
            item["url"] = response.url
            item["listable"] = jsonresponse["badges"][badge]["listable"]
            item["manuallyGrantable"] = jsonresponse["badges"][badge]["manually_grantable"]
            item["multipleGrant"] = jsonresponse["badges"][badge]["multiple_grant"]
            item["name"] = jsonresponse["badges"][badge]["name"]
            item["grantCount"] = jsonresponse["badges"][badge]["grant_count"]
            item["description"] = jsonresponse["badges"][badge]["description"]
            item["badgeGroupingId"] = jsonresponse["badges"][badge]["badge_grouping_id"]
            item["badgeTypeId"] = jsonresponse["badges"][badge]["badge_type_id"]
            yield item
