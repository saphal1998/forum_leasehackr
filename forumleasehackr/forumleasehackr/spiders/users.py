# -*- coding: utf-8 -*-
import scrapy
import pandas as pd
import json
from forumleasehackr.items import ForumleasehackrItem

usernames = pd.read_csv('/Users/saphalpatro/Desktop/leasehackr/MembersData_processed.csv', sep=',', encoding='utf-8')

usernames = usernames["Username"]
# print(usernames)

# Run "scrapy crawl users -t csv -o users.csv --loglevel=INFO" from terminal after reaching spider folder to save files


class UsersSpider(scrapy.Spider):
    name = 'users'
    allowed_domains = ['forum.leasehackr.com']
    start_urls = ['https://forum.leasehackr.com']

    def start_requests(self):
        for user in usernames:
            userData = self.start_urls[0] + '/u/' + user + '.json'
            yield scrapy.Request(userData, callback=self.parse_userData)

    def parse_userData(self, response):
        self.logger.info('Recieved response from {}'.format(response.url))
        jsonresponse = json.loads(response.body)
        item = ForumleasehackrItem()
        item["username"] = jsonresponse["user"]["username"]
        item["url"] = response.url
        item["joinedAt"] = jsonresponse["user"]["created_at"]
        item["lastSeenAt"] = jsonresponse["user"]["last_seen_at"]
        item["invitedBy"] = jsonresponse["user"]["invited_by"]
        item["status"] = jsonresponse["user"]["title"]
        item["profileViewCount"] = jsonresponse["user"]["profile_view_count"]
        item["lastPostedAt"] = jsonresponse["user"]["last_posted_at"]
        item["timeRead"] = jsonresponse["user"]["time_read"]
        item["trustLevel"] = jsonresponse["user"]["trust_level"]
        item["badgeCount"] = jsonresponse["user"]["badge_count"]
        yield item
