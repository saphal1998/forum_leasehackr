# -*- coding: utf-8 -*-
import scrapy
import json
from forumleasehackr.items import BadgeDetails

# Run "scrapy crawl badgeInfo -t csv -o badgeInfo.csv --loglevel=INFO" from terminal after reaching spider folder to save files


class BadgeinfoSpider(scrapy.Spider):
    name = 'badgeInfo'
    allowed_domains = ['forum.leasehackr.com']
    start_urls = ['http://forum.leasehackr.com/badges.json']

    def parse(self, response):
        jsonresponse = json.loads(response.body)
        for badge in jsonresponse["badges"]:
            item = BadgeDetails()
            item["allowTitle"] = badge["allow_title"]
            item["badgeGroupingId"] = badge["badge_grouping_id"]
            item["badgeTypeId"] = badge["badge_type_id"]
            item["description"] = badge["description"]
            item["enabled"] = badge["enabled"]
            item["grantCount"] = badge["grant_count"]
            item["id"] = badge["id"]
            item["listable"] = badge["listable"]
            item["longDescription"] = badge["long_description"]
            item["manuallyGrantable"] = badge["manually_grantable"]
            item["multipleGrant"] = badge["multiple_grant"]
            item["name"] = badge["name"]
            print("Details of ", badge["name"], " obtained")
            yield item
