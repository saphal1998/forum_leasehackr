from selenium import webdriver
from bs4 import BeautifulSoup
import time
import pandas as pd
import numpy as np
import re

SCROLL_PAUSE_TIME = 30


def handle_data(bsObj):
    columns = ["Username", "Hearts Received", "Hearts Given", "Topics Created", "Replies Posted", "Topics Viewed", "Posts Read", "Days Visited", "Time Read", "Status"]
    usernames = bsObj.findAll("span", {"class": "username"})
    aspects = bsObj.findAll("span", {"class": "number"})
    time_read = bsObj.findAll("span", {"class": "time-read"})
    titles = bsObj.findAll("div", {"class": "title"})
    regex_pattern = r'>.*<'

    namesOfUsers = []
    for username in usernames:
        namesOfUsers.append(username.find("a").attrs["href"][3:])
    namesOfUsers = pd.DataFrame(namesOfUsers)

    heartsReceived = []
    heartsGiven = []
    topicsCreated = []
    repliesPosted = []
    topicsViewed = []
    postsRead = []
    daysVisited = []
    temp = 0
    for number in aspects:
        number = re.findall(regex_pattern, str(number))
        if temp % 7 == 0:
            temp += 1
            heartsReceived.append(number)
            continue
        if temp % 7 == 1:
            heartsGiven.append(number)
            temp += 1
            continue
        if temp % 7 == 2:
            topicsCreated.append(number)
            temp += 1
            continue
        if temp % 7 == 3:
            repliesPosted.append(number)
            temp += 1
            continue
        if temp % 7 == 4:
            topicsViewed.append(number)
            temp += 1
            continue
        if temp % 7 == 5:
            postsRead.append(number)
            temp += 1
            continue
        if temp % 7 == 6:
            daysVisited.append(number)
            temp += 1

    heartsReceived = np.array(heartsReceived)
    heartsGiven = np.array(heartsGiven)
    topicsCreated = np.array(topicsCreated)
    repliesPosted = np.array(repliesPosted)
    topicsViewed = np.array(topicsViewed)
    postsRead = np.array(postsRead)
    daysVisited = np.array(daysVisited)
    userData = pd.DataFrame(np.column_stack((heartsReceived, heartsGiven, topicsCreated, repliesPosted, topicsViewed, postsRead, daysVisited)))

    timeRead = []
    for time in time_read:
        time = re.findall(regex_pattern, str(time))
        timeRead.append(time)
    timeRead = np.array(timeRead)
    timeRead = pd.DataFrame(timeRead)

    temp = 0
    statusUser = []
    for status in titles:
        status = re.findall(regex_pattern, str(status))
        if temp == 0:
            temp += 1
            continue
        statusUser.append(status)
        temp += 1
    statusUser = np.array(statusUser)
    statusUser = pd.DataFrame(statusUser)
    memberData = pd.concat([namesOfUsers, userData, timeRead, statusUser], axis=1)
    memberData.to_csv("MemberData.csv", sep='\t', encoding='utf-8')

    return memberData


driver = webdriver.PhantomJS(executable_path="/Users/saphalpatro/Downloads/phantomjs-2.1.1-macosx/bin/phantomjs")
driver.get("https://forum.leasehackr.com/u?period=all")
number_of_users = driver.find_element_by_class_name("total-rows").text
print("There are " + str(number_of_users) + " users on site")
while True:
    last_height = driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(SCROLL_PAUSE_TIME)
    bsObj = BeautifulSoup(driver.page_source)
    memberData = handle_data(bsObj)
    print(len(memberData), "members are stored!")
    if(len(memberData) >= number_of_users):
        break


driver.close()
print("Scraping of the webpage is done!")
